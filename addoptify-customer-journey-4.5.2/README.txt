Customer Journey Plugin®
========================
Copyright 2017 Addoptify ApS

# Info

Help your clients unite best-practice CRM with their customers’ decision journeys.
A new enterprise solution infuses the customer’s entire decision process with internal efficiency to streamline processes,
boost sales performance and strengthen customer engagement across all departments.

Check out SugarExchange for more info: https://sugarexchange.sugarcrm.com/apps/44/customer-journey-plug-in

# Installation

1. Uninstall the old package if it was installed before from the Sugar Module Loader
2. Install the package using the Sugar Module Loader
3. Enter your license key and activate users under http://sugar-url/#DRI_Workflows/layout/configuration

Check out the Installation Guide for more details: http://support.sugarcrm.com/Documentation/Installable_Connectors/Customer_Journey_Plug-in/Customer_Journey_Plug-In_Installation_Guide

# Configuration

The plugin comes with 8 templates

View/update/create your templates under http://sugar-url/#DRI_Workflow_Templates

Check out the Administration Guide for more details: http://support.sugarcrm.com/Documentation/Installable_Connectors/Customer_Journey_Plug-in/Customer_Journey_Plug-In_Administration_Guide

# Usage

1. Navigate to a Account, Contact, Lead, Opportunity or Case
2. Select your template of your choice and click on Start
3. Tick of the tasks to complete the journey

Check out the User Guide for more details: http://support.sugarcrm.com/Documentation/Installable_Connectors/Customer_Journey_Plug-in/Customer_Journey_Plug-In_User_Guide
