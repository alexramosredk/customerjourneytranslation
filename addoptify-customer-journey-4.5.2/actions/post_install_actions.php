<?php

require_once 'modules/Administration/QuickRepairAndRebuild.php';
$db = DBManagerFactory::getInstance();

/**
 * Start with performing a quick repair.
 *
 * We need to do two repairs in order to install the modules properly in
 * all Sugar versions (one repair does not work in Sugar v7.6.2.1).
 */

echo "Performing first quick repair & rebuild ... <br>\n";
$GLOBALS['log']->info('CJ Install: Performing first quick repair & rebuild');

// the first should not execute any queries, this is what fails
// when installing the package for the first time in Sugar v7.6.2.1
$autoexecute = true;
$show_output = false;
$randc = new RepairAndClear();
$randc->repairAndClearAll(
    array('clearAll'),
    array(translate('LBL_ALL_MODULES')),
    $autoexecute,
    $show_output
);

echo "Performing second quick repair & rebuild ... <br>\n";
$GLOBALS['log']->info('CJ Install: Performing second quick repair & rebuild');

$autoexecute = true;
$show_output = false;
$randc = new RepairAndClear();
$randc->repairAndClearAll(
    array('clearAll'),
    array(translate('LBL_ALL_MODULES')),
    $autoexecute,
    $show_output
);

echo "Migrating calls table ... <br>\n";
ensure_column_exist('calls', 'dri_workflow_sort_order',       array ('type' => 'varchar', 'default' => 1)); // 'VARCHAR(255) DEFAULT 1 NULL'
ensure_column_exist('calls', 'customer_journey_points',       array ('type' => 'int', 'len'=> 3, 'default' => 10)); // 'INT(3) DEFAULT 10 NULL'
ensure_column_exist('calls', 'customer_journey_score',        array ('type' => 'int', 'len'=> 3)); // 'INT(3) DEFAULT 10 NULL'
ensure_column_exist('calls', 'customer_journey_progress',     array ('type' => 'float')); // 'FLOAT NULL'
ensure_column_exist('calls', 'dri_workflow_task_template_id', array ('type' => 'id')); // 'CHAR(36) NULL'
ensure_column_exist('calls', 'dri_subworkflow_id',            array ('type' => 'id')); // 'CHAR(36) NULL'
ensure_column_exist('calls', 'dri_subworkflow_template_id',   array ('type' => 'id')); // 'CHAR(36) NULL'
ensure_column_exist('calls', 'dri_workflow_template_id',      array ('type' => 'id')); // 'CHAR(36) NULL'
ensure_column_exist('calls', 'is_customer_journey_activity',  array ('type' => 'bool', 'default' => false)); // 'BOOL DEFAULT 0 NULL'
ensure_column_exist('calls', 'customer_journey_parent_activity_id',    array ('type' => 'id')); // 'CHAR(36) NULL'
ensure_column_exist('calls', 'customer_journey_parent_activity_type',  array ('type' => 'varchar')); // 'VARCHAR(255) NULL'
ensure_column_exist('calls', 'is_customer_journey_parent_activity',    array ('type' => 'bool', 'default' => false)); // 'BOOL DEFAULT 0 NULL'
ensure_column_exist('calls', 'customer_journey_blocked_by',            array ('type' => 'json', 'dbType' => 'text')); // 'text  NULL'

ensure_idx_exist('calls', 'idx_dri_workflow_task_template_id', array ('dri_workflow_task_template_id'));
ensure_idx_exist('calls', 'idx_dri_subworkflow_id',            array ('dri_subworkflow_id'));
ensure_idx_exist('calls', 'idx_dri_subworkflow_template_id',   array ('dri_subworkflow_template_id'));
ensure_idx_exist('calls', 'idx_dri_workflow_template_id',      array ('dri_workflow_template_id'));

echo "Migrating meetings table ... <br>\n";
ensure_column_exist('meetings', 'dri_workflow_sort_order',       array ('type' => 'varchar', 'default' => 1)); // 'VARCHAR(255) DEFAULT 1 NULL'
ensure_column_exist('meetings', 'customer_journey_points',       array ('type' => 'int', 'len' => 3, 'default' => 10)); // 'INT(3) DEFAULT 10 NULL'
ensure_column_exist('meetings', 'customer_journey_score',        array ('type' => 'int', 'len'=> 3)); // 'INT(3) DEFAULT 10 NULL'
ensure_column_exist('meetings', 'customer_journey_progress',     array ('type' => 'float')); // 'FLOAT NULL'
ensure_column_exist('meetings', 'dri_workflow_task_template_id', array ('type' => 'id')); // 'CHAR(36) NULL'
ensure_column_exist('meetings', 'dri_subworkflow_id',            array ('type' => 'id')); // 'CHAR(36) NULL'
ensure_column_exist('meetings', 'dri_subworkflow_template_id',   array ('type' => 'id')); // 'CHAR(36) NULL'
ensure_column_exist('meetings', 'dri_workflow_template_id',      array ('type' => 'id')); // 'CHAR(36) NULL'
ensure_column_exist('meetings', 'is_customer_journey_activity',  array ('type' => 'bool', 'default' => false)); // 'BOOL DEFAULT 0 NULL'
ensure_column_exist('meetings', 'customer_journey_parent_activity_id',    array ('type' => 'id')); // 'CHAR(36) NULL'
ensure_column_exist('meetings', 'customer_journey_parent_activity_type',  array ('type' => 'varchar')); // 'VARCHAR(255) NULL'
ensure_column_exist('meetings', 'is_customer_journey_parent_activity',    array ('type' => 'bool', 'default' => false)); // 'BOOL DEFAULT 0 NULL'
ensure_column_exist('meetings', 'customer_journey_blocked_by',            array ('type' => 'json', 'dbType' => 'text')); // 'text  NULL'

ensure_idx_exist('meetings', 'idx_dri_workflow_task_template_id', array ('dri_workflow_task_template_id'));
ensure_idx_exist('meetings', 'idx_dri_subworkflow_id',            array ('dri_subworkflow_id'));
ensure_idx_exist('meetings', 'idx_dri_subworkflow_template_id',   array ('dri_subworkflow_template_id'));
ensure_idx_exist('meetings', 'idx_dri_workflow_template_id',      array ('dri_workflow_template_id'));

echo "Migrating tasks table ... <br>\n";
ensure_column_exist('tasks', 'dri_workflow_sort_order',       array ('type' => 'varchar', 'default' => 1)); // 'VARCHAR(255) DEFAULT 1 NULL'
ensure_column_exist('tasks', 'customer_journey_points',       array ('type' => 'int', 'len' => 3, 'default' => 10)); // 'INT(3) DEFAULT 10 NULL'
ensure_column_exist('tasks', 'customer_journey_score',        array ('type' => 'int', 'len'=> 3)); // 'INT(3) DEFAULT 10 NULL'
ensure_column_exist('tasks', 'customer_journey_progress',     array ('type' => 'float')); // 'FLOAT NULL'
ensure_column_exist('tasks', 'customer_journey_type',         array ('type' => 'varchar', 'len' => 255, 'default' => 'customer_task')); // 'VARCHAR(255) DEFAULT \'customer_task\' NULL'
ensure_column_exist('tasks', 'dri_workflow_task_template_id', array ('type' => 'id')); // 'CHAR(36) NULL'
ensure_column_exist('tasks', 'dri_subworkflow_id',            array ('type' => 'id')); // 'CHAR(36) NULL'
ensure_column_exist('tasks', 'dri_subworkflow_template_id',   array ('type' => 'id')); // 'CHAR(36) NULL'
ensure_column_exist('tasks', 'dri_workflow_template_id',      array ('type' => 'id')); // 'CHAR(36) NULL'
ensure_column_exist('tasks', 'is_customer_journey_activity',  array ('type' => 'bool', 'default' => false)); // 'BOOL DEFAULT 0 NULL'
ensure_column_exist('tasks', 'customer_journey_parent_activity_id',    array ('type' => 'id')); // 'CHAR(36) NULL'
ensure_column_exist('tasks', 'customer_journey_parent_activity_type',  array ('type' => 'varchar')); // 'VARCHAR(255) NULL'
ensure_column_exist('tasks', 'is_customer_journey_parent_activity',    array ('type' => 'bool', 'default' => false)); // 'BOOL DEFAULT 0 NULL'
ensure_column_exist('tasks', 'customer_journey_blocked_by',            array ('type' => 'json', 'dbType' => 'text')); // 'text  NULL'

ensure_idx_exist('tasks', 'idx_dri_workflow_task_template_id', array ('dri_workflow_task_template_id'));
ensure_idx_exist('tasks', 'idx_dri_subworkflow_id',            array ('dri_subworkflow_id'));
ensure_idx_exist('tasks', 'idx_dri_subworkflow_template_id',   array ('dri_subworkflow_template_id'));
ensure_idx_exist('tasks', 'idx_dri_workflow_template_id',      array ('dri_workflow_template_id'));

ensure_column_removed('tasks', 'is_dri_workflow_task');

echo "Migrating accounts table ... <br>\n";
ensure_column_exist('accounts', 'dri_workflow_template_id', array ('type' => 'id')); // 'CHAR(36) NULL'
ensure_idx_exist('accounts', 'idx_dri_workflow_template_id', array ('dri_workflow_template_id'));

echo "Migrating contacts table ... <br>\n";
ensure_column_exist('contacts', 'dri_workflow_template_id', array ('type' => 'id')); // 'CHAR(36) NULL'
ensure_idx_exist('contacts', 'idx_dri_workflow_template_id', array ('dri_workflow_template_id'));

echo "Migrating leads table ... <br>\n";
ensure_column_exist('leads', 'dri_workflow_template_id', array ('type' => 'id')); // 'CHAR(36) NULL'
ensure_idx_exist('leads', 'idx_dri_workflow_template_id',   array ('dri_workflow_template_id'));

echo "Migrating cases table ... <br>\n";
ensure_column_exist('cases', 'dri_workflow_template_id', array ('type' => 'id')); // 'CHAR(36) NULL'
ensure_idx_exist('cases', 'idx_dri_workflow_template_id',   array ('dri_workflow_template_id'));

echo "Migrating opportunities table ... <br>\n";
ensure_column_exist('opportunities', 'dri_workflow_template_id', array ('type' => 'id')); // 'CHAR(36) NULL'
ensure_idx_exist('opportunities', 'idx_dri_workflow_template_id', array ('dri_workflow_template_id'));

echo "Migrating users table ... <br>\n";
ensure_column_exist('users', 'customer_journey_access', array ('type' => 'bool', 'default' => false)); // 'BOOL DEFAULT 0 NULL'

if (table_exist('dri_workflows')) {
    echo "Migrating dri_workflows table ... <br>\n";
    ensure_column_renamed('dri_workflows', 'acase_id', 'case_id');

    ensure_column_exist('dri_workflows', 'parent_id',        array ('type' => 'id')); // 'CHAR(36) NULL'
    ensure_column_exist('dri_workflows', 'opportunity_id',   array ('type' => 'id')); // 'CHAR(36) NULL'
    ensure_column_exist('dri_workflows', 'account_id',       array ('type' => 'id')); // 'CHAR(36) NULL'
    ensure_column_exist('dri_workflows', 'lead_id',          array ('type' => 'id')); // 'CHAR(36) NULL'
    ensure_column_exist('dri_workflows', 'case_id',          array ('type' => 'id')); // 'CHAR(36) NULL'
    ensure_column_exist('dri_workflows', 'current_stage_id', array ('type' => 'id')); // 'CHAR(36) NULL'
    ensure_column_exist('dri_workflows', 'contact_id',       array ('type' => 'id')); // 'CHAR(36) NULL'

    ensure_idx_exist('dri_workflows', 'idx_parent_id',        array ('parent_id'));
    ensure_idx_exist('dri_workflows', 'idx_opportunity_id',   array ('opportunity_id'));
    ensure_idx_exist('dri_workflows', 'idx_account_id',       array ('account_id'));
    ensure_idx_exist('dri_workflows', 'idx_lead_id',          array ('lead_id'));
    ensure_idx_exist('dri_workflows', 'idx_case_id',          array ('case_id'));
    ensure_idx_exist('dri_workflows', 'idx_current_stage_id', array ('current_stage_id'));
    ensure_idx_exist('dri_workflows', 'idx_contact_id',       array ('contact_id'));

    ensure_idx_removed('dri_workflows', 'idx_acase_id');
    ensure_column_removed('dri_workflows', 'acase_id');

    ensure_column_exist('dri_workflow_task_templates', 'sort_order', array ('type' => 'varchar', 'default' => 1)); // 'VARCHAR(255) DEFAULT 1 NULL'
    ensure_column_exist('dri_workflow_task_templates', 'points', array ('type' => 'int', 'len' => 3, 'default' => 10)); // 'INT(3) DEFAULT 10 NULL'

    /** Then perform migrations between the major versions **/

    echo "Migrating from CJ v3.x to v4.x ... <br>\n";
    $GLOBALS['log']->info('CJ Install: Migrating from CJ v3.x to v4.x ...');

    $sql = 'SELECT * FROM dri_workflows WHERE deleted = 0';
    $result = $db->query($sql);

    $GLOBALS['log']->info('CJ Install: Moving activities to correct parent ...');
    while ($journey = $db->fetchByAssoc($result)) {
        if (!empty($journey['account_id'])) {
            $parent_id = $journey['account_id'];
            $parent_type = 'Accounts';
        } elseif (!empty($journey['contact_id'])) {
            $parent_id = $journey['contact_id'];
            $parent_type = 'Contacts';
        } elseif (!empty($journey['lead_id'])) {
            $parent_id = $journey['lead_id'];
            $parent_type = 'Leads';
        } elseif (!empty($journey['acase_id'])) {
            $parent_id = $journey['acase_id'];
            $parent_type = 'Cases';
        } elseif (!empty($journey['opportunity_id'])) {
            $parent_id = $journey['opportunity_id'];
            $parent_type = 'Opportunities';
        } else {
            continue;
        }

        $sql = "SELECT * FROM dri_subworkflows WHERE dri_workflow_id = '%s' AND deleted = 0";
        $sql = sprintf($sql, $journey['id']);
        $result2 = $db->query($sql);

        while ($stage = $db->fetchByAssoc($result2)) {
            foreach (array ('Tasks', 'Meetings', 'Calls') as $module)
            {
                $sql = "SELECT * FROM %s WHERE deleted = 0 AND parent_type = '%s' AND parent_id = '%s'";
                $sql = sprintf($sql, strtolower($module), 'DRI_SubWorkflows', $stage['id']);
                $res = $db->query($sql);
                while ($activity = $db->fetchByAssoc($res)) {
                    if ($activity['parent_id'] != $parent_id || $activity['parent_type'] != $parent_type || $activity['dri_subworkflow_id'] != $stage['id'])
                    {
                        $sql = "UPDATE %s SET parent_id = '%s', parent_type = '%s', dri_subworkflow_id = '%s' WHERE id = '%s'";
                        $sql = sprintf(
                            $sql,
                            strtolower($module),
                            $parent_id,
                            $parent_type,
                            $stage['id'],
                            $activity['id']
                        );

                        cj_query($sql);
                    }
                }
            }
        }
    }

    if (table_has_column('dri_workflow_templates', 'points')) {
        cj_query('UPDATE dri_subworkflow_templates
SET dri_subworkflow_templates.points = (
  SELECT SUM(a.points)
  FROM dri_workflow_task_templates a
  WHERE a.deleted = 0 AND a.dri_subworkflow_template_id = dri_subworkflow_templates.id
), dri_subworkflow_templates.related_activities = (
  SELECT COUNT(a.id)
  FROM dri_workflow_task_templates a
  WHERE a.deleted = 0
    AND a.dri_subworkflow_template_id = dri_subworkflow_templates.id
) WHERE dri_subworkflow_templates.deleted = 0');

        cj_query('UPDATE dri_workflow_templates
SET dri_workflow_templates.points = (
	SELECT SUM(a.points)
	FROM dri_subworkflow_templates a
	WHERE a.deleted = 0
	  AND a.dri_workflow_template_id = dri_workflow_templates.id
), dri_workflow_templates.related_activities = (
	SELECT SUM(a.related_activities)
	FROM dri_subworkflow_templates a
	WHERE a.deleted = 0
	  AND a.dri_workflow_template_id = dri_workflow_templates.id
) WHERE dri_workflow_templates.deleted = 0');
    }
}

echo "Migrating from CJ v4.0 to v4.1 ... <br>\n";
$GLOBALS['log']->info('CJ Install: Migrating from CJ v4.0 to v4.1 ...');
cj_query('UPDATE tasks SET is_customer_journey_activity = 1 WHERE dri_subworkflow_id IS NOT NULL AND is_customer_journey_activity = 0');
cj_query('UPDATE calls SET is_customer_journey_activity = 1 WHERE dri_subworkflow_id IS NOT NULL AND is_customer_journey_activity = 0');
cj_query('UPDATE meetings SET is_customer_journey_activity = 1 WHERE dri_subworkflow_id IS NOT NULL AND is_customer_journey_activity = 0');

if (table_exist('dri_workflow_task_templates')) {
    echo "Migrating dri_workflow_task_templates table ... <br>\n";
    ensure_column_exist('dri_workflow_task_templates', 'dri_workflow_template_id', array ('type' => 'id')); // 'CHAR(36) NULL'
    ensure_column_exist('dri_workflow_task_templates', 'blocked_by_id',            array ('type' => 'id')); // 'CHAR(36) NULL'
    ensure_column_exist('dri_workflow_task_templates', 'blocked_by',               array ('type' => 'text')); // 'TEXT NULL'
    ensure_column_exist('dri_workflow_task_templates', 'is_parent',                array ('type' => 'bool', 'default' => false)); // 'BOOL DEFAULT 0 NULL'
    ensure_column_exist('dri_workflow_task_templates', 'send_invites',             array ('type' => 'varchar', 'len' => 255, 'default' => 'none')); // 'VARCHAR(255) DEFAULT \'none\' NULL'

    ensure_idx_exist('dri_workflow_task_templates', 'idx_dri_workflow_template_id', array ('dri_workflow_template_id'));
    ensure_idx_exist('dri_workflow_task_templates', 'idx_blocked_by_id', array ('blocked_by_id'));

    cj_query('UPDATE dri_workflow_task_templates SET dri_workflow_template_id = (SELECT dri_workflow_template_id FROM dri_subworkflow_templates s WHERE s.id = dri_workflow_task_templates.dri_subworkflow_template_id);');
    cj_query("UPDATE dri_workflow_task_templates SET blocked_by = CONCAT('[\"', blocked_by_id, '\"]') WHERE blocked_by_id != '' AND blocked_by_id IS NOT NULL AND blocked_by IS NULL");
}

echo "Migrating from CJ v4.4 to v4.5 ... <br>\n";
$GLOBALS['log']->info('CJ Install: Migrating from CJ v4.4 to v4.5 ...');
cj_query('UPDATE tasks SET customer_journey_score = customer_journey_points WHERE is_customer_journey_activity = 1 AND is_customer_journey_parent_activity = 0 AND (customer_journey_parent_activity_id = \'\' OR customer_journey_parent_activity_id IS NULL) AND status IN (\'Completed\', \'Not Applicable\')');
cj_query('UPDATE tasks SET customer_journey_score = customer_journey_points * 0.3 WHERE is_customer_journey_activity = 1 AND is_customer_journey_parent_activity = 0 AND (customer_journey_parent_activity_id = \'\' OR customer_journey_parent_activity_id IS NULL) AND status NOT IN (\'Not Started\', \'Completed\', \'Not Applicable\')');
cj_query('UPDATE tasks SET customer_journey_score = 0 WHERE is_customer_journey_activity = 1 AND is_customer_journey_parent_activity = 0 AND (customer_journey_parent_activity_id = \'\' OR customer_journey_parent_activity_id IS NULL) AND status = \'Not Started\'');

cj_query('UPDATE calls SET customer_journey_score = customer_journey_points WHERE is_customer_journey_activity = 1 AND is_customer_journey_parent_activity = 0 AND (customer_journey_parent_activity_id = \'\' OR customer_journey_parent_activity_id IS NULL) AND status IN (\'Held\', \'Not Held\')');
cj_query('UPDATE calls SET customer_journey_score = customer_journey_points * 0.3 WHERE is_customer_journey_activity = 1 AND is_customer_journey_parent_activity = 0 AND (customer_journey_parent_activity_id = \'\' OR customer_journey_parent_activity_id IS NULL) AND status NOT IN (\'Planned\', \'Held\', \'Not Held\')');
cj_query('UPDATE calls SET customer_journey_score = 0 WHERE is_customer_journey_activity = 1 AND is_customer_journey_parent_activity = 0 AND (customer_journey_parent_activity_id = \'\' OR customer_journey_parent_activity_id IS NULL) AND status = \'Planned\'');

cj_query('UPDATE meetings SET customer_journey_score = customer_journey_points WHERE is_customer_journey_activity = 1 AND is_customer_journey_parent_activity = 0 AND (customer_journey_parent_activity_id = \'\' OR customer_journey_parent_activity_id IS NULL) AND status IN (\'Held\', \'Not Held\')');
cj_query('UPDATE meetings SET customer_journey_score = customer_journey_points * 0.3 WHERE is_customer_journey_activity = 1 AND is_customer_journey_parent_activity = 0 AND (customer_journey_parent_activity_id = \'\' OR customer_journey_parent_activity_id IS NULL) AND status NOT IN (\'Planned\', \'Held\', \'Not Held\')');
cj_query('UPDATE meetings SET customer_journey_score = 0 WHERE is_customer_journey_activity = 1 AND is_customer_journey_parent_activity = 0 AND (customer_journey_parent_activity_id = \'\' OR customer_journey_parent_activity_id IS NULL) AND status = \'Planned\'');

cj_query('UPDATE tasks SET customer_journey_progress = customer_journey_score / customer_journey_points WHERE is_customer_journey_activity = 1');
cj_query('UPDATE calls SET customer_journey_progress = customer_journey_score / customer_journey_points WHERE is_customer_journey_activity = 1');
cj_query('UPDATE meetings SET customer_journey_progress = customer_journey_score / customer_journey_points WHERE is_customer_journey_activity = 1');

cj_query('UPDATE tasks SET dri_subworkflow_template_id = (SELECT dri_subworkflow_template_id FROM dri_subworkflows s WHERE s.id = tasks.dri_subworkflow_id)');
cj_query('UPDATE tasks SET dri_workflow_template_id = (SELECT dri_workflow_template_id FROM dri_subworkflow_templates s WHERE s.id = tasks.dri_subworkflow_template_id)');

cj_query('UPDATE calls SET dri_subworkflow_template_id = (SELECT dri_subworkflow_template_id FROM dri_subworkflows s WHERE s.id = calls.dri_subworkflow_id)');
cj_query('UPDATE calls SET dri_workflow_template_id = (SELECT dri_workflow_template_id FROM dri_subworkflow_templates s WHERE s.id = calls.dri_subworkflow_template_id)');

cj_query('UPDATE meetings SET dri_subworkflow_template_id = (SELECT dri_subworkflow_template_id FROM dri_subworkflows s WHERE s.id = meetings.dri_subworkflow_id)');
cj_query('UPDATE meetings SET dri_workflow_template_id = (SELECT dri_workflow_template_id FROM dri_subworkflow_templates s WHERE s.id = meetings.dri_subworkflow_template_id)');

echo "Migrating from CJ v4.5.0 to v4.5.1 ... <br>\n";
$sql = "SELECT id, blocked_by, blocked_by_id, activity_type FROM dri_workflow_task_templates WHERE (blocked_by IS NOT NULL OR blocked_by_id IS NOT NULL) AND deleted = 0";
$result = $db->query($sql);

$activityTableMap = array (
    'Tasks' => 'tasks',
    'Meetings' => 'meetings',
    'Calls' => 'calls',
);

while ($row = $db->fetchByAssoc($result)) {
    if (!empty($row['blocked_by'])) {
        $value = $db->decodeHTML($row['blocked_by']);
    } elseif (!empty($row['blocked_by_id'])) {
        $value = '["'.$row['blocked_by_id'].'"]';
    } else {
        continue;
    }

    $sql = 'UPDATE %s SET customer_journey_blocked_by = \'%s\' WHERE dri_workflow_task_template_id = \'%s\'';
    $sql = sprintf($sql, $activityTableMap[$row['activity_type']], $db->quote($value), $row['id']);
    cj_query($sql);
}

/** Rebuild relationships **/

echo "Rebuilding relationships ... <br>\n";
$GLOBALS['log']->info('CJ Install: Rebuilding relationships ...');
SugarRelationshipFactory::rebuildCache();

/** Repair roles **/

echo "Repairing roles ... <br>\n";
$GLOBALS['log']->info('CJ Install: Repairing roles ...');
require "modules/ACL/install_actions.php";

if (table_exist('dri_workflow_templates') && SugarAutoLoader::fileExists('modules/DRI_Workflow_Templates/ControlPanel.php')) {
    try {
        echo "\nImporting templates ... <br>\n";
        $GLOBALS['log']->info('CJ Install: Importing templates ...');
        require_once "modules/DRI_Workflow_Templates/ControlPanel.php";
        $panel = new \DRI_Workflow_Templates\ControlPanel();
        $panel->importTemplates();
    } catch (\Exception $e) {

    }
}

/* Utility functions */

/**
 * @param string $table
 * @param string $column
 * @return bool
 */
function table_has_column($table, $column)
{
    $db = DBManagerFactory::getInstance();
    $columns = $db->get_columns($table);
    return isset($columns[$column]);
}

/**
 * @param string $table
 * @param string $index
 * @return bool
 */
function table_has_idx($table, $index)
{
    $db = DBManagerFactory::getInstance();
    $indices = $db->get_indices($table);
    return isset($indices[$index])
        && count($indices[$index]) !== 0;
}

/**
 * @param string $table
 * @return bool
 */
function table_exist($table)
{
    $db = DBManagerFactory::getInstance();
    return $db->tableExists($table);
}

/**
 * @param string $table
 * @param string $index
 * @param array $fields
 */
function ensure_idx_exist($table, $index, array $fields)
{
    if (!table_has_idx($table, $index)) {
        add_idx($table, $index, $fields);
    }
}

/**
 * @param string $table
 * @param string $column
 * @param array $def
 */
function ensure_column_exist($table, $column, array $def)
{
    if (!table_has_column($table, $column)) {
        add_column($table, $column, $def);
    }
}

/**
 * @param string $table
 * @param string $column
 */
function ensure_column_removed($table, $column)
{
    if (table_has_column($table, $column)) {
        remove_column($table, $column);
    }
}

/**
 * @param string $table
 * @param string $index
 */
function ensure_idx_removed($table, $index)
{
    $db = DBManagerFactory::getInstance();
    if (table_has_idx($table, $index)) {
        $sql = $db->dropIndexes($table, array (array ('name' => $index)), false);
        cj_query($sql);
    }
}

/**
 * @param string $table
 * @param string $column
 */
function remove_column($table, $column)
{
    $db = DBManagerFactory::getInstance();
    $sql = $db->dropColumnSQL($table, array (array ('name' => $column)));
    cj_query($sql);
}

/**
 * @param string $table
 * @param string $old
 * @param string $new
 */
function rename_column($table, $old, $new)
{
    $db = DBManagerFactory::getInstance();
    $sql = $db->renameColumnSQL($table, $old, $new);
    cj_query($sql);
}

/**
 * @param string $table
 * @param string $column
 * @param array $def
 */
function add_column($table, $column, array $def)
{
    $db = DBManagerFactory::getInstance();
    $def['name'] = $column;
    $sql = $db->addColumnSQL($table, array ($def));
    cj_query($sql);
}

/**
 * @param string $table
 * @param string $index
 * @param array $fields
 */
function add_idx($table, $index, array $fields)
{
    $db = DBManagerFactory::getInstance();
    $indexes = array (array ('name' => $index, 'type' => 'index', 'fields' => $fields));
    $sql = $db->addIndexes($table, $indexes, false);
    cj_query($sql);
}

/**
 * @param string $table
 * @param string $old
 * @param string $new
 */
function ensure_column_renamed($table, $old, $new)
{
    if (table_has_column($table, $old) && !table_has_column($table, $new)) {
        rename_column($table, $old, $new);
    }
}

/**
 * @param string $sql
 * @return bool|resource
 */
function cj_query($sql)
{
    $db = DBManagerFactory::getInstance();
    echo " >>> $sql<br>\n";
    return $db->query($sql, true);
}
