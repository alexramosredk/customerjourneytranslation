<?php

echo "Performing a quick repair & rebuild ... <br>\n";

$autoexecute = true;
$show_output = false;
require_once("modules/Administration/QuickRepairAndRebuild.php");
$randc = new RepairAndClear();
$randc->repairAndClearAll(
    array('clearAll'),
    array(translate('LBL_ALL_MODULES')),
    $autoexecute,
    $show_output
);

echo "Removing config settings ... <br>\n";
$db = DBManagerFactory::getInstance();
$sql = "DELETE FROM config WHERE category = 'DRI_Workflows' AND name = 'template_version'";
$db->query($sql);
