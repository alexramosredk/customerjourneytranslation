<?php

$app_strings['LBL_DRI_SUBWORKFLOWS'] = 'Etapas Customer Journey';
$app_strings['LBL_DRI_SUBWORKFLOW'] = 'Etapa Customer Journey';
$app_strings['LBL_DRI_SUBWORKFLOW_TEMPLATES'] = 'Plantillas de Etapas Customer Journey';
$app_strings['LBL_DRI_SUBWORKFLOW_TEMPLATE'] = 'Plantilla de Etapa Customer Journey';
$app_strings['LBL_DRI_WORKFLOWS'] = 'Customer Journeys';
$app_strings['LBL_DRI_WORKFLOW'] = 'Customer Journey';
$app_strings['LBL_DRI_WORKFLOW_TASK_TEMPLATES'] = 'Plantillas de Actividad Customer Journey';
$app_strings['LBL_DRI_WORKFLOW_TASK_TEMPLATE'] = 'Plantilla de Actividad Customer Journey';
$app_strings['LBL_DRI_WORKFLOW_TEMPLATES'] = 'Plantillas Customer Journey';
$app_strings['LBL_DRI_WORKFLOW_TEMPLATE'] = 'Plantilla Customer Journey';
$app_strings['LBL_DEFAULT_DRI_CUSTOMER_JOURNEY_DASHLET_TITLE'] = 'Customer Journey';
$app_strings['LBL_DEFAULT_DRI_CUSTOMER_JOURNEY_DASHLET_DESC'] = 'Muestra el gráfico circular de Customer Journey.';
$app_strings['LBL_ASSIGN_ME_BUTTON_TITLE'] = 'Asignarme';
$app_strings['LBL_COMPLETE_BUTTON_TITLE'] = 'Completar';
$app_strings['LBL_DELETE_STAGE_BUTTON_TITLE'] = 'Eliminar Etapa';
$app_strings['LBL_START_CYCLE_BUTTON_TITLE'] = 'Iniciar';
$app_strings['LBL_NOT_APPLICABLE_BUTTON_TITLE'] = 'No Aplica';
$app_strings['LBL_ADD_STAGE_BUTTON_TITLE'] = 'Añadir Etapa';
$app_strings['LBL_CONFIGURE_TEMPLATE_BUTTON_TITLE'] = 'Configurar Plantilla';
$app_strings['LBL_ADD_CJ_SUB_TASK_BUTTON_TITLE'] = 'Añadir Tarea';
$app_strings['LBL_ADD_CJ_SUB_MEETING_BUTTON_TITLE'] = 'Programar Reunión';
$app_strings['LBL_ADD_CJ_SUB_CALL_BUTTON_TITLE'] = 'Programar Llamada';
$app_strings['LBL_CUSTOMER_JOURNEY_ACTIVITY_DUE_DATE_FUTURE'] = 'es en el futuro';
$app_strings['LBL_CUSTOMER_JOURNEY_ACTIVITY_DUE_DATE_OVERDUE'] = 'está vencido';
$app_strings['LBL_CUSTOMER_JOURNEY_ACTIVITY_DUE_DATE_TODAY'] = 'es hoy';
$app_strings['LBL_CUSTOMER_JOURNEY_ACTIVITY_DUE_DATE_TOMORROW'] = 'es mañana';
$app_strings['LBL_CUSTOMER_JOURNEY_ACTIVITY_DUE_DATE_TEXT'] = '{{fieldName}} {{status}}
{{formatUser}} ({{fromNow}})';
$app_strings['LBL_CUSTOMER_JOURNEY_ERROR_LICENSE_KEY_USER_LIMIT_REACHED'] = 'Límite de Usuario Alcanzado';
$app_strings['LBL_CUSTOMER_JOURNEY_ERROR_INVALID_LICENSE_KEY'] = 'Clave de Licencia Inválida';
$app_strings['LBL_CUSTOMER_JOURNEY_ERROR_INVALID_APPLICATION_LICENSE_KEY'] = 'Solicitud Inválida';
$app_strings['LBL_CUSTOMER_JOURNEY_ERROR_MISSING_LICENSE_KEY'] = 'Solicitud Inválida';
$app_strings['LBL_CUSTOMER_JOURNEY_ERROR_MISSING_VALIDATION_KEY'] = 'Falta la Clave de Validación';
$app_strings['LBL_CUSTOMER_JOURNEY_ERROR_CORRUPT_VALIDATION_KEY'] = 'Clave de Validación Corrupta';
$app_strings['LBL_CUSTOMER_JOURNEY_ERROR_LICENSE_KEY_EXPIRED'] = 'Licencia Expirada';
$app_strings['LBL_CUSTOMER_JOURNEY_ERROR_LICENSE_KEY_MISSING_INFO'] = 'Falta Información';
$app_strings['LBL_CUSTOMER_JOURNEY_ERROR_USER_MISSING_ACCESS'] = 'Usuario sin Acceso';
$app_strings['LBL_CUSTOMER_JOURNEY_INVALID_LICENSE'] = 'Licencia Inválida';
$app_strings['LBL_CUSTOMER_JOURNEY_LICENSE_ABOUT_TO_EXPIRE_NAME'] = 'Customer Journey: la licencia está a punto de caducar';
$app_strings['LBL_CUSTOMER_JOURNEY_LICENSE_ABOUT_TO_EXPIRE_DESCRIPTION'] = 'Su licencia de Customer Journey expirará {0} y necesita ser renovada<br>
<br>
Por favor contactar en <a href="mailto:support@addoptify.com">support@addoptify.com</a> para extender la licencia y obtener una nueva clave de validación.';
$app_strings['LBL_CUSTOMER_JOURNEY_USER_LIMIT_REACHED_NAME'] = 'Customer Journey: límite de usuario alcanzado';
$app_strings['LBL_CUSTOMER_JOURNEY_USER_LIMIT_REACHED_DESCRIPTION'] = 'El límite de usuario de su Customer Journey {0} se ha excedido<br>
<br>
Por favor contactar en <a href="mailto:support@addoptify.com">support@addoptify.com</a> para aumentar el límite del usuario o disminuir el número de usuarios que usan el plugin.';

$app_list_strings['moduleList']['DRI_SubWorkflows'] = 'Etapas Customer Journey';
$app_list_strings['moduleList']['DRI_SubWorkflow_Templates'] = 'Plantillas de Etapas Customer Journey';
$app_list_strings['moduleList']['DRI_Workflows'] = 'Customer Journeys';
$app_list_strings['moduleList']['DRI_Workflow_Task_Templates'] = 'Plantillas de Actividad Customer Journey';
$app_list_strings['moduleList']['DRI_Workflow_Templates'] = 'Plantillas Customer Journey';

$app_list_strings['moduleListSingular']['DRI_SubWorkflows'] = 'Etapa Customer Journey';
$app_list_strings['moduleListSingular']['DRI_SubWorkflow_Templates'] = 'Plantilla de Etapa Customer Journey';
$app_list_strings['moduleListSingular']['DRI_Workflows'] = 'Customer Journey';
$app_list_strings['moduleListSingular']['DRI_Workflow_Task_Templates'] = 'Plantilla de Actividad Customer Journey';
$app_list_strings['moduleListSingular']['DRI_Workflow_Templates'] = 'Plantilla Customer Journey';

$app_list_strings['dri_workflow_task_templates_type_list'] = array (
    '' => '',
    'customer_task' => 'Tarea Cliente',
    'milestone' => 'Hito',
    'internal_task' => 'Tarea Interna',
    'agency_task' => 'Tarea Agencia',
    'automatic_task' => 'Tarea Automática',
);

$app_list_strings['dri_workflow_task_templates_activity_type_list'] = array (
    'Tasks' => 'Tarea',
    'Calls' => 'LLamada',
    'Meetings' => 'Reunión',
);

$app_list_strings['dri_workflow_templates_available_modules_list'] = array (
    'Leads' => 'Cliente Potencial',
    'Accounts' => 'Cuenta',
    'Contacts' => 'Contacto',
    'Cases' => 'Caso',
    'Opportunities' => 'Oportunidad',
);

$app_list_strings['dri_workflow_task_templates_points_list'] = array (
    '10' => '10',
    '9' => '9',
    '8' => '8',
    '7' => '7',
    '6' => '6',
    '5' => '5',
    '4' => '4',
    '3' => '3',
    '2' => '2',
    '1' => '1',
);

$app_list_strings['dri_subworkflows_state_list'] = array (
    'not_started' => 'No Iniciado',
    'in_progress' => 'En Progreso',
    'not_completed' => 'No Completado',
    'completed' => 'Completado',
);

$app_list_strings['dri_workflow_task_templates_task_due_date_type_list'] = array (
    '' => '',
    'days_from_created' => 'Días Desde Creación CJ',
    'days_from_stage_started' => 'Dias Desde Estapa Iniciada',
    'days_from_previous_activity_completed' => 'Días Desde Actividad Previa Completada',
);

$app_list_strings['dri_workflows_state_list'] = array (
    'not_started' => 'No Iniciado',
    'in_progress' => 'En Progreso',
    'completed' => 'Completado',
);

$app_list_strings['task_status_dom']['Not Applicable'] = 'No Aplicable';

$app_list_strings['dri_workflows_parent_type_list'] = array (
    'Tasks' => 'Tarea Actual',
    'Calls' => 'Llamada Actual',
    'Meetings' => 'Reunión Actual',
);

$app_list_strings['dri_workflow_templates_assignee_rule_list'] = array (
    'none' => 'Ninguna',
    'create' => 'En la Creación',
    'stage_start' => 'En el Inicio de la Etapa',
);

$app_list_strings['dri_workflow_templates_target_assignee_list'] = array (
    'current_user' => 'Usuario Actual',
    'parent_assignee' => 'Asignación Principal',
);

$app_list_strings['dri_workflow_task_templates_send_invites_list'] = array (
    'none' => 'None',
    'create' => 'En la Creación',
    'stage_start' => 'En el Inicio de la Etapa',
);

$app_list_strings['dri_customer_journey_parent_activity_type_list'] = array (
    'Tasks' => 'Tarea',
    'Meetings' => 'Reunión',
    'Calls' => 'Llamada',
);
