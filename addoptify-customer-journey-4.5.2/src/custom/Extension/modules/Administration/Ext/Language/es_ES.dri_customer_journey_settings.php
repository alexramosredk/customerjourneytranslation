<?php

$mod_strings['LBL_DRI_CUSTOMER_JOURNEY_SETTINGS_TITLE'] = 'Configuración Customer Journey';
$mod_strings['LBL_DRI_CUSTOMER_JOURNEY_SETTINGS_DESC'] = 'Configuración para el paquete Customer Journey';
$mod_strings['LBL_DRI_CUSTOMER_JOURNEY_SETTINGS_LINK_NAME'] = 'Licencia y Validación';
$mod_strings['LBL_DRI_CUSTOMER_JOURNEY_SETTINGS_LINK_DESC'] = 'Configurar claves de licencia y validación';
$mod_strings['LBL_DRI_CUSTOMER_JOURNEY_TEMPLATES_LINK_NAME'] = 'Plantillas';
$mod_strings['LBL_DRI_CUSTOMER_JOURNEY_TEMPLATES_LINK_DESC'] = 'Gestionar Plantillas';
$mod_strings['LBL_DRI_CUSTOMER_JOURNEY_CONFIGURE_MODULES_LINK_NAME'] = 'Configurar Módulos';
$mod_strings['LBL_DRI_CUSTOMER_JOURNEY_CONFIGURE_MODULES_LINK_DESC'] = 'Configurar módulos habilitados';
