<?php

$mod_strings['LBL_DRI_WORKFLOW_SORT_ORDER'] = 'Ordenación Customer Journey';
$mod_strings['LBL_CUSTOMER_JOURNEY_POINTS'] = 'Puntos Customer Journey';
$mod_strings['LBL_IS_CUSTOMER_JOURNEY_ACTIVITY'] = 'Es una Llamada Customer Journey';
$mod_strings['LBL_CURRENT_CUSTOMER_JOURNEY_ACTIVITY_AT'] = 'Actividad actual del Customer Journey en';
$mod_strings['LBL_CUSTOMER_JOURNEY_SCORE'] = 'Puntuación Customer Journey';
$mod_strings['LBL_CUSTOMER_JOURNEY_PROGRESS'] = 'Progreso Customer Journey';
$mod_strings['LBL_CUSTOMER_JOURNEY_PARENT_ACTIVITY_TYPE'] = 'Tipo de Actividad padre del Customer Journey';
$mod_strings['LBL_CUSTOMER_JOURNEY_PARENT_ACTIVITY_ID'] = 'Id de Actividad Padre del Customer Journey';
$mod_strings['LBL_IS_CUSTOMER_JOURNEY_PARENT_ACTIVITY'] = 'Es una Actividad Padre de Customer Journey';
$mod_strings['LBL_CUSTOMER_JOURNEY_BLOCKED_BY'] = 'Customer Journey Bloqueado Por';



