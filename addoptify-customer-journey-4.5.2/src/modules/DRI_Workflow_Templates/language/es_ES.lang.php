<?php

$mod_strings = array (



  'LBL_CREATED' => 'Creado por',
  'LBL_CREATED_ID' => 'Creado por ID',
  'LBL_CREATED_USER' => 'Creado por el usuario ',
  'LBL_DATE_ENTERED' => 'Fecha de creación',
  'LBL_DATE_MODIFIED' => 'Fecha de modificación',
  'LBL_DELETED' => 'Eliminado',
  'LBL_DESCRIPTION' => 'Descripción',
  'LBL_EDIT_BUTTON' => 'Editar',
  'LBL_HOMEPAGE_TITLE' => 'Mis Customer Journey abiertos',
  'LBL_ID' => 'ID',
  'LBL_LIST_FORM_TITLE' => 'Lista de Customer Journey',
  'LNK_IMPORT_CUSTOMER_JOURNEY_TEMPLATES' => 'Importa Customer Journey Plantillas',
  'LBL_LIST_NAME' => 'Nombre',
  'LBL_MODIFIED' => 'Modificado por',
  'LBL_MODIFIED_ID' => 'Modificado por Id',
  'LBL_MODIFIED_NAME' => 'Modificado por nombre',
  'LBL_MODIFIED_USER' => 'Modificado por Usuario',
  'LBL_MODULE_TITLE' => 'Customer Journey',
  'LBL_MODULE_NAME' => 'Customer Journeys',
  'LBL_NAME' => 'Nombre',
  'LBL_NEW_FORM_TITLE' => 'Nuevo Customer Journey Plantillas',
  'LBL_REMOVE' => 'Eliminar',
  'LBL_SEARCH_FORM_TITLE' => 'Buscar Customer Journey Plantillas',
  'LBL_TEAM' => 'Equipo',
  'LBL_TEAMS' => 'Equipos',
  'LBL_TEAM_ID' => 'Equipo Id',
  'LBL_TYPE' => 'Tipo',
  'LNK_LIST' => 'Customer Journey Plantillas',
  'LNK_NEW_RECORD' => 'Crear Customer Journey Plantillas',
  'LBL_COPIES' => 'Copias',
  'LBL_COPIED_TEMPLATE' => 'Plantilla Copiada',
  'LBL_IMPORT_TEMPLATES_BUTTON_LABEL' => 'Importar Plantillas',
  'LBL_IMPORT_TEMPLATES_SUCCESS_MESSAGE' => 'La plantilla ha sido importada',
  'LBL_RESAVE_TEMPLATES_BUTTON_LABEL' => 'Volver a guardar plantillas',
  'LBL_RESAVE_TEMPLATES_SUCCESS_MESSAGE' => 'Las plantillas han sido guardadas',
  'LNK_VIEW_RECORDS' => 'Vista de Customer Journey Plantillas',
  'LBL_AVAILABLE_MODULES' => 'Módulos disponibles',
  'LBL_POINTS' => 'Puntos',
  'LBL_RELATED_ACTIVITIES' => 'Actividades relacionadas',
  'LBL_ACTIVE' => 'Activo',
  'LBL_ASSIGNEE_RULE' => 'Regla Asignada',
  'LBL_TARGET_ASSIGNEE' => 'Objetivo Asignado',
  'LBL_EXPORT_BUTTON_LABEL' => 'Exportar',
  'LBL_CUSTOMER_JOURNEY_TEMPLATE_IMPORT_BUTTON_LABEL' => 'Importar',
  'LBL_CUSTOMER_JOURNEY_TEMPLATE_IMPORT_TEXT' => 'Cree/actualice automáticamente un nuevo registro de Customer Journey importando un archivo * .json de su sistema de archivos.',
  'LBL_CUSTOMER_JOURNEY_TEMPLATE_IMPORT_CREATE_SUCCESS' => 'Plantilla <a href="#{{buildRoute model=this module="DRI_Workflow_Templates"}}">{{name}}</a> ha sido creada.',
  'LBL_CUSTOMER_JOURNEY_TEMPLATE_IMPORT_UPDATE_SUCCESS' => 'Plantilla <a href="#{{buildRoute model=this module="DRI_Workflow_Templates"}}">{{name}}</a>  ha sido creada.',
  'LBL_CUSTOMER_JOURNEY_TEMPLATE_IMPORT_DUPLICATE_NAME_ERROR' => 'Importación Fallida. La plantilla llamada "<a href="#{{buildRoute model=this module="DRI_Workflow_Templates"}}">{{name}}</a>" ya existe. Cambie el nombre del registro importado y vuelva a intentarlo o use "Copiar" para crear una plantilla de Journey duplicada. ',
  'LBL_CUSTOMER_JOURNEY_TEMPLATE_IMPORT_UPDATE_CONFIRM' => 'Ya existe una plantilla con esta ID. Para actualizar la plantilla existente, haga clic <b>Confirm</b>. Para salir sin realizar ningún cambio en la plantilla existente, haga clic en <b>Cancel</b>.',
  'LBL_CUSTOMER_JOURNEY_TEMPLATE_EMPTY_WARNING' => 'Por favor seleccione un fichero *.json válido.',
  'LBL_CHECKING_IMPORT_UPLOAD' => 'Validando',
  'LBL_IMPORTING_TEMPLATE' => 'Importando',
);
